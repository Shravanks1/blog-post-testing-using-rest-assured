package com.isaac.dto;

public record UserSignInDTO(String email, String password) {}
