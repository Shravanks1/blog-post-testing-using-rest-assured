package com.isaac.dto;

public record PostDTO(String title, String content) {
}
