package com.isaac.dto;

public record UserSignUpDTO(String email, String name, String password) {}
